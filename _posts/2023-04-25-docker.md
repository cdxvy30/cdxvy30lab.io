---
layout: post
title: Docker
author: cdxvy30
categories: security
tags: [docker]
---
# Docker v.s. VM
- 容器: 以應用程式為主軸的虛擬化
- VM: 以作業系統為主軸的虛擬化
- 圖

# VMM and VM
- 不論實體機器上跑了什麼其他東西，都要符合實體虛擬一致的要求。
- VM可能有虛擬裝置
- VMM 不局限於 Time-Sharing System

# LXC (Linux Container)
- OS-level Virtualization

# Container技術四本柱
- Chroot
- UnionFS
- Namespace
- Cgroup(?)

# chroot
- Change root
  - `chroot <new_root_dir>`

# UnionFS
- Def: 實作能把目錄同時掛載在同一個目錄底下的檔案系統

# Terminology
- Image之於Container=Program之於Process

# Hello World
- 好像要先把container移除才能移除image
- `docker pull <image_name>`: 從docker hub拉image下來
- `docker run`: 跑image 找不到會去自動pull
- `docker rm <id/name>`: remove container
- `docker rmi <id/name>`: remove image

# Dockerfile
- `\`: 上一行指令的延續
- `FROM <image>`: 選定Base image
- `RUN <command>`: 執行指令
- `WORKDIR <directory>`: 指定工作目錄, 可能會搭配一些用到路徑的指令
- `COPY . .`: 複製檔案
- `ENV <var>=<name>`: 設定環境變數(注意不能有空白)
- `CMD []`: container被啟動時執行的指令/執行檔
  - 可以直接用Binary的Entrypoint或C的main()去理解
- `ENTRYPOINT`: 指定container啟動時執行的檔案

- 概念
  - 每一句指令都會建立一個container layer，執行完每個layer後會壓縮成一個layer，變成一個image layer。

# Lab 0x01 & 0x02
1. 自幹Python腳本 + 一個Dockerfile
- [參考資源](https://www.tutorialworks.com/python-develop-container/)

2. C/C++
  - 請自己寫一個C code 並讓他自動執行
- 

- `docker build <path_to_Dockerfile>`: 
- 如何執行&Attach Container內的Shell
  - `docker exec -it <container_id> /bin/bash`

# docker-compose
- One-liner建構環境
- 可以想成一次執行多個`Dockerfile`

- `docker-compose up -d`: 第一次
- `docker-compose down -d`: 砍掉
- `docker-compose start -d`: 跑過
- `docker-compose stop -d`: 停止

- 支援的檔案類型
  - `docker-compose.yml`
  - `docker-compose.yaml`