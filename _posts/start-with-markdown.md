---
layout: post
title: 撰寫開發紀錄--從Markdown開始
author: cdxvy30
categories: techniques
tags: [Markdown]
---

## 本站點使用主題
其實在中研院實習第二次了...第一次是大二升三的暑假，在
雖然說Jekyll原生支援透過Markdown撰寫，但還是有一些些語法不支援(當初此框架的設計者考量)，但目前也只發現Headers六階層文字的第一階層不支援轉換而已，其他就等撰寫文章過程中慢慢去摸索了。

## 介紹Markdown
Markdown語法是一種美學(?)，讓使用者可以專注在撰寫文字本身，不必被排版軟體各種花式制約(對就是在說Microsoft Word)，也更容易傳遞資料本身，因為Markdown本身語法簡潔的特性，使其可以透過命令列工具，如`pandoc`，快速將Markdown編寫的檔案轉換成特定格式文件，例如:`.doc`, `.pdf`等等。

## 語法

#### 粗體/斜體(Italics/Bold)
在一般文書處理軟體中，要讓文字產生變化，例如粗體、斜體、底線、螢光註釋等，通常是用GUI操作(當然也有快捷鍵或快捷轉換方式)，但在純文字編輯、沒有GUI輔助環境下，Markdown可以很快速的由常用的標示符(Symbols)標示出要產生變化的文字。
* 範例
```markdown
In this sentence, I'll display how to make the word **bold** or __bold__.
Besides, this is an example of _italic_ or *italic*.
```
效果如下：
In this sentence, I'll display how to make the word **bold** or __bold__.
Besides, this is an example of _italic_ or *italic*.

簡單來說，粗體是在文字前後方各加上兩個星號(`*`)或底線(`_`)，斜體則為一個。
花式一點的用法也可以同時讓文字呈現粗斜體，
* 範例
```markdown
This **_word_** is displayed in both **bold** and _italic_.
Also, you can use it in this way: __*word*__.
Noticed that you cannot display it in ___word___, but you can do it like ***word***.
```
效果如下：
This **_word_** is displayed in both **bold** and _italic_.
Also, you can use it in this way: __*word*__.
Noticed that you cannot display it in ___word___, but you can do it like ***word***.

## 標題
撰寫文章時除了最主要的標題之外，通常也需要幾個副標題、甚至更下階層的小標題，以階層式的架構讓整篇文章看起來更有組織。Markdown語法中簡單透過不同數量的(`#`)就可以達到不同階層標題的目的。
* 範例

## 參考
[MarkdownTutorial]: https://www.markdowntutorial.com
[Markdown]:  