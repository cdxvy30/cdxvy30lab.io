---
layout: post
title: Read.Note-Google的軟體工程之道:Introduction
subheading: Updating
author: cdxvy30
categories: reading
tags: [software engineering]
---
## 動機
近期認識到自己滿需要讀一些課外讀物度過不想做正事的時間，不然很多時間都被自己浪費掉；很多大大的板上都推薦讀Google的軟體工程之道，剛好前陣子朋友送我這本書，在一邊掙扎著寫研討會論文的同時決定讀一下這本軟體工程的課外讀物。

## 前言
大略翻了一下目錄和序，看起來這本書討論的內容比較偏向介紹「Google內部的工作文化」，而不是一本技術文件；簡單來說作者想要呈現的是在Google這樣龐大的軟體公司內部，是如何生產、維護多個軟體的codebase，透過什麼樣的工具、法則可以較好的協助軟體工程師在開發過程中提升速率以及提高程式碼的可維護性。

## 