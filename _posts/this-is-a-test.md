---
layout: post
title: This is a test
subheading: For syntax and functionality in this theme. Strength in numbers.
author: cdxvy30
categories: markdown
banner:
  image: https://blog.deliveringhappiness.com/hs-fs/hubfs/case-study-golden-state-warriors.jpg?width=1582&height=888&name=case-study-golden-state-warriors.jpg
  opacity: 0.3
  background: "#000"
  height: "100vh"
  min_height: "38vh"
  heading_style: "font-size: 4.25em; font-weight: bold; text-decoration: underline"
  subheading_style: "color: gold"
tags: [markdown, code block]
sidebar: [] # Don't know what's its impact
---

For the test post, I'll write a some test code block.

- - -

## C

**Bold paragraph**
*Italic font*
Jekyll provides support for code snippets:

{% highlight c %}
#include<stdio.h>

int add(int a, int b);

int main () {
  int a, b;
  scanf("%d %d", &a, &b);
  printf("%d", add(a, b));
  return 0;
}

int add(int a, int b) {
  return (a + b);
}
{% endhighlight %}

## Linked List
#### Singly linked list
Singly linked list means that there is only one way to manipulate the list. 
#### Doubly linked list
Doubly linked list means that the list is bidirectional.
> You can manipulate list in two-way. 
- - -

## C++

Another way to write code snippets:

```cpp
#include <iostream>
using namespace std;

int main() {
  cout << "Hello World!";
  return 0;
}
```

```javascript
console.log("Hello, World!");
```

* Web Development
  * Front-end
    * Framework
      1. React.js
      2. Vue.js
      3. Angular.js
  * Back-end
    * Database
      1. PostgreSQL
      2. MySQL
    * Server
      * Express.js

### Code block

    Code blocks are very useful for developers and other people who look at code or other things that are written in plain text. As you can see, it uses a fixed-width font.

You can also make `inline code` to add code into other things.

### Quote

> Here is a quote. What this is should be self explanatory. Quotes are automatically indented when they are used.

### Headings *can* also contain **formatting**

### They can even contain `inline code`

# Don't know why the first heading (h1) become the smallest.