---
layout: post
title: Dev.Diary-管理終端機session工具:學習用tmux提升工作效率
subheading: Updating
author: cdxvy30
categories: develop
tags: [linux, tool]
---
## 前言

## 安裝
### MacOs
- 首先安裝 `homebrew` (多數人應該都有，省略安裝過程)
- `brew install tmux`
- 執行`tmux`，若終端機視窗下方有出現綠色狀態列代表成功
### WSL
### Ubuntu

## 基本概念
`tmux`強大的地方在於可以在同一個終端機session下切割畫面，讓使用者不用多開終端機視窗，這點不管在WSL/MacOS/Linux應該都很受用。由於開發的關係常常會需要多個畫面處理不同事情(例如一個畫面開`vim`另一個畫面執行程式看output)，或者需要不同session同時執行環境設定與在該環境下開發。
### Session
每次執行`tmux`指令都會產生新的`session`，等同於平常開不同終端機執行緒，一個`session`等於一個執行緒。
### Window
`window`就是執行`tmux`指令後看到的視窗，一個`session`下可以有多個`window`，通常可以在狀態列中看到不同`window`的訊息。
### Pane
一個`window`下又可以切分為多個`pane`區塊，就是前面所提及的"切割畫面"。

## 常用指令
- `tmux list-sessions` or `tmux ls` -> 列出現有session
- `tmux attach -t <session number> or <session name>` -> 進入指定session

### Hot Keys
- 所有快捷鍵必須透過前置鍵 prefix key (`<pre>`)觸發，預設是`ctrl-b`，也可以自行配製成`ctrl-a`(較好按)。
- `<pre> + "` -> 執行視窗水平分割
- `<pre> + %` -> 執行視窗垂直分割
- `<pre> + d` -> 將該session放到背景執行
- `<pre> + x` -> 關閉目前的pane
- `<pre> + [` -> 切換到scroll view，`q`離開並回到指令模式



## 總結
撰寫服務相當常需要在terminal間遨遊，如果只會透過多開terminal視窗來解決的話，大概開到3個甚至2個以上就相當不方便，因此能熟悉這個工具

其實tmux本身外觀配置很陽春，實務上可以搭配`oh-my-tmux`美化這個工具的外觀，應該會寫另一篇文章分享怎麼配置`tmux`的外觀。

## 參考資料
- [tmux Cheatsheet](https://gist.github.com/MohamedAlaa/2961058)
