---
layout: post
title: Class.Note-資料庫系統lec2:ER Model
author: cdxvy30
categories: class
tags: [database systems]
---
# What is ER model?
- ER model是由台裔美籍科學家陳品山先生所提出，在資料庫領域扮演重要角色(但不限於應用在資料庫領域)。
- 幫助系統設計者抽象化並建構系統內各實體的關聯，以設計正確的系統。

# ER Model的關注點以及為何使用
ER model的重點在於**如何建立一個entity的模型**以及**如何透過relationship去關聯不同實體**。注意在設計ER model的階段不需要考慮到技術和實作細節的層面，避免陷入特定資料庫管理系統、語言或技術的框架限制；也就是說，在概念設計(conceptual design)階段，更重要的是如何抽象化的建立整個application中各個元件的角色，並將他們關聯起來，完成設計之後，再來考慮要用我們熟悉的Relational/Document/Graph等等的Data Model來實作。

> 使用ER model可以幫助我們從high-level的角度來設計Database，將application中的各個requirement實體與關係建立起來，而不必考慮技術和實作細節。

# ER Model
- 實體(Entity)
  - 一件事物的集合(set)
  - 可用一個屬性集合來描述
- 屬性(Attribute)
  - 用來描述實體
  - 具有atomic domain特性，也就是各個屬性有各自的型別，例如:string, integer等
- 關聯(Relationship)
  - 實體間的關聯

# Entity
## Entity Type, Set and Instance(有印象就好)
因為只是名詞上的定義，不是很重要，直接舉例:
- Employee: is an entity type.
- "Cody": is an entity instance (of the entity type Employee).
- {"Cody", "Ryan", "Adam"}: is an entity set.

# Attribute
## Attribute values的種類
- Single-valued
- Multi-valued: 其值包含多個不可分割的值
- Simple value
- Composite value: 由其他不可分割值的欄位組合成的值
  - 要設計成simple or composite只是design issue，沒有必然
- Storaged value
- Derived value: 可從其他欄位推估出的值
- **Null**: 有三種情況會視為`null`
  - 不知道值是否存在
  - 值不存在，且未知
  - 值不適用此欄位
  - `null`的出現代表application的開發者需要思考如何處理上述特定情況

## Key Attributes of an Entity Type
在各個instance中皆存在，且每個instance帶的值都不同，具唯一性，稱為**Key attribute**。也稱為**unique constraint**。

## Attribute的數學定義
假設`E`代表一個實體，`V`代表值的集合，`P(V)`代表`V`的超集合(包含所有`V`的子集合與空集合)，而此時屬性`A`就可以被定義成E map to P(V)的function，記法如下:
- `A: E -> P(V)`
- 可以想成: 一個實體`E`的`A`屬性`A(E)`會對應的值集合`P(V)`中的哪些值，可能會對應到一個值或多個值；而`A`也可以不只是一個屬性，所以也有多對多的可能性。

# Relationship
在ER model中，兩個實體間的關係最初可能是一個實體中參照到另一個實體的屬性，而這樣的屬性會被獨立出來設計為所謂的`relationship`，因此隨著設計推動，關聯會漸由一個實體中的屬性轉換回兩個實體間的關聯。

## Relationship Type, Set and Instance(有印象就好)
一樣，因為只是名詞上的定義，不是很重要，直接舉例:
- The possibility of the student registering in department form a relationship **type**.
- "Cody" registers in dept. of "CE", is a relationship **instance**.
- {("Cody", "CE"), ("Ryan", "CS"), ("Adam", "EE")} is a relationship **set**.

## Relationship的數學定義
(待補)

## Degree of Relationships
參與關係的實體數量，最常見的就是兩個實體間的關聯，其`degree of relationships == 2`。實務上最常見的最高只到`3`。

## Role Name
用以指明兩個實體在一組關係中扮演的角色。不常用，僅在recursive relationship中為必要。例如:有`Student`和`Department`兩個實體，透過`enroll`關聯，`Student`在這段關係中的角色為`study in` (department)，而`Department`在這段關係中的角色為`have` (student)

## Recursive Relationship
指有同一個Entity參與同一個Relationship，此時**Role Name**就可以幫助我們理解Entity在Relationship中的角色。

> Recursive Relationship在現實世界中的應用很常發生。

## Constraints of Relationship Types (重要!)
現實世界中的Relationship不可能毫無限制，因此在ER Model中透過設計**constraint**的方式限制Entity間的關聯，常見的constraint有下列兩種:
- Cardinality ratio
- Participation

## Cardinality ratio
只有三種可能:
- 1:1
- 1:N/N:1
- N:M

> 在cardinality ratio中的數字比例，代表的是在一個`relationship instance`中`entity instance`的數量

## Participation
- Total Participation
  - Also called **existence dependency**
  - 每個entity instance必須至少參與一個relationship中
- Partial Participation
  - Also called **Non-total participation**
  - 即上者的反義

> Cardinality ratio和Participation constraints合稱為Structural Constraints，因為他們定義了Relationship的架構。

## Weak Entity
自身不帶有**key attributes**，需要透過一個關聯到其他Entity(稱為**identifying , owner, dominating or parent entity type**)的relationship(稱為**identifying relationship**)和一個自身的attribute(稱為**partial key**)作為辨識依據。而Weak entity自身又稱為subordinate entity type or child entity type。

> 實務上還是前面所談的Strong Entity Type較常見。

# Enhanced ER Model (EER Model)
因應漸趨複雜的現實應用(如:CAD, GIS, etc.)，一般的ER model逐漸無法應付將現實概念化的需求，因此有了EER model的出現。本質上就是引入OOP中`superclass/subclass`的概念，以及透過`inheritance`讓ER model有更彈性的概念化能力。

想像現在有一家公司，內部員工分為銷售員、工程師和秘書三種職位，而員工就是工程師的`superclass`，秘書則是員工的`subclass`。這樣的關係在之前的ER model中無法適當的概念化。

## Type Inheritance
繼承的概念在物件導向程式語言中如`C++`相當常見，一樣的概念應用在EER model中也適用。`subclass`會繼承`superclass`中的所有屬性(attributes)和關聯(relationships)，而同時`subclass`也可以擁有屬於自己的local attributes，例如上述的工程師可能會有「擅長的程式語言」這個屬性，而銷售員則會有「業績」這個屬性，這也是ER model中沒辦法實現的繼承概念化。

## Specialization and Generalization
進一步談`superclass/subclass`之間的**繼承**關係，在EER的世界中是透過`Specialization`(特化)和`Generalization`(泛化)來達成。

Specialization就是定義`subclass`繼承`superclass`的過程，也可想成specialize後的結果。一個`superclass`可根據不同的特徵而有不同的特化方式。例如`employee`根據`job_type`特化出`{engineers, sales, secretary}`，或根據`salary_type`特化出`{hourly_emp, monthly_emp}`。

Generalization就是特化的相反，因一般都是談如何特化，這裡就不贅述。

## Multiple Specialization
一個`superclass`或`subclass`皆可參與多個繼承關係，而繼承也可能是多層級(hierarchy)的架構。

## Subclass是如何被定義的?
主要分兩種方式:
- Predicated-defined subclasses
  - 在`superclass`中有一個屬性用以決定`subclass`實體的名稱，這個attribute稱為`defining predicate`；舉前面以Employee為`superclass`，Engineers, Sales, Secretary為`subclass`的例子，如果在Employee中用`is_secretary`或`job_type`這樣的屬性來定義subclass的話，subclass就屬於**Predicated-defined subclasses**。
    - Attribute-defined specialization: 是Predicated-defined subclasses中的特例；前述的`job_type`可以直接述明所定義的`subclass`，因此又叫做`defining attribute`。
- User-defined subclasses
  - 相對的，在`superclass`中沒有用來定義`subclass`的屬性，而是由user自行定義subclass。(實務上似乎較少出現)

## Constraints on Specialization / Generalization
- Disjoint
  - 分為`disjoint`和`overlapping`，前者代表「一個Entity最多只能屬於一個subclass」，後者則是多個。
- Completeness
  - 分為`total`和`partial`，前者代表「superclass內的每個實體必須至少屬於一個subclass」，後者則無此限制。

## Union and Categories
基本上是為了彌補EER無法表達superclass多於subclass的可能性。透過Union，可將不同Entity type的實體形成一個新的集合。這樣的集合稱為`Union type`或`category`。

## 備註
這個單元應該要放些說明圖比較好理解...不過目前還沒想到比較好上傳並host圖片的方式(有人是用Amazon S3啦...不過沒錢)