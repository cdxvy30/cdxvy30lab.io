---
layout: post
title: 參與台灣雲原生推廣--詞彙表翻譯
author: cdxvy30
categories: community
tags: [cncf]
---
## 參與社群
從參與COSCUP開始，能夠參與社群、回饋社群就一直是夢想之一(?)

4/25晚上剛好看到雲原生社群在徵人做在地化推廣，大概猶豫了3分鐘還是報名了。也有幸拿到小飛機的LinkedIn(咦?)

## 工作內容
主要就是針對雲原生運算基金會的[詞彙表](https://glossary.cncf.io/)做翻譯，因為台灣目前還沒有人做，需要有人去開branch來當志工。

這兩天就是由台灣的approver去開了一個issue來啟動繁體中文化的專案，目前正在討論branch的名字要怎麼取，後來決定應該是根據ISO 639的協議。

之後就等approver去開branch，應該就可以開始認領詞彙做翻譯。

## 要準備的事
- 要熟悉PR/開issue的機制，有點久沒做了，要找時間複習。
- 另外可以了解一下`git`裡面`submodule`的運作，因為今天有自己clone整個詞彙表的[repo](https://github.com/cncf/glossary)下來試著run hugo server看看。

## 參考資源
- [Initiate Traditional Chinese Localization Team](https://github.com/cncf/glossary/issues/1987)
- [ISO 639-2](https://www.loc.gov/standards/iso639-2/php/code_list.php)
- [Localization.md](https://github.com/cncf/glossary/blob/main/LOCALIZATION.md#initiating-a-new-localization-team)