---
layout: post
title: Dev.Diary-GCE排程和開機自動化腳本設定
subheading: Updating
author: cdxvy30
categories: develop
tags: [cloud]
---
## 前言
剛好在碩班接下一個產學合作案，要開發一個較具規模的後端服務，需要自行規劃資料庫並開發RESTful API、Socket、雲端部署、自動化流程等(對沒錯整個後端只有一個人...)。

這個案子除了要開發後端服務外，還要介接畢業學長所寫的機器學習模型到整個服務架構內，考量到我們整個team內沒有人熟悉雲端服務，所以決定將負責處理機器學習任務的服務和處理所有其他任務的服務分開在兩台虛擬主機上，也就是開了兩台GCE。但這樣造成的後果就是，在雲端服務上開銷太大(一個月將近8000，且還只是在開發階段沒有實際使用者)，最近決定將處理其他任務的伺服器遷移到處理機器學習任務的主機上，並且設定主機每日開關機排程，希望能最大幅度降低成本。

## 排程
排程的部分參考![萬里雲的教學](https://mile.cloud/zh/resources/blog/gcp-vm-manager-schedule-a-vm-instance-to-start-and-stop_428)，在Google Cloud Platform上的GUI介面從Compute Engine -> Instance Schedules -> Create Schedule就可以設定排程的時間、頻率等。

注意設定前要先針對Compute Engine Service Agent設定角色權限。

## 自動化腳本
這部分我將開機需要啟用的服務和對應的指令告訴ChatGPT，請他幫忙生成一個範例腳本。需要啟用的服務包括Redis Server、Node.js (Express) Server, Flask App。其實滿直覺的就是將三個服務分別進入對應資料夾後下指令即可，ChatGPT生成的範例腳本如下:
```bash
#!/bin/bash
redis-server  # 啟動Redis Server
sleep 5   # 因Node.js服務依賴於Redis，需要時間等redis run起來，否則Node.js服務會出現error直接死掉
cd ~/path/to/server
npm start &   # 啟動Node.js服務
cd ~/path/to/another/server
python application.py &   # 啟動Flask App
```
當下檢查覺得沒有問題，我就直接丟到GCE上中繼資料start-script的欄位並測試，果然出錯。

## 路徑問題
注意前面的`cd`指令中的`~`，我花了一段時間才發現原本以為是`home` directory的位置在GCE中其實是`/`(root)中的root，也就是該符號其實代表的是`/root`而不是`/home/username`，至於發現的原因是透過`sudo journalctl -u google-startup-scripts.service`指令去看GCE在跑startup腳本時出現的log。

## 問題排查
先簡單紀錄一下過程中用來排查問題的一些指令(嗚嗚嗚Linux command好重要):
1. Process
  - `ps -ef`
  - `kill -9 [PID]`
2. PORT
  - `netstat -tulpn`
  - `netstat -tulpn | grep LISTEN`

## 套件相依性和執行環境問題
過程中有發現從startup腳本執行的程式和平時下指令起server執行的程式不同，真的幸好有用`ps`和`sudo journalctl -u google-startup-scripts.service`去看log。平常用`npm start`時，都是從`/home/[username]/.nvm/versions/node/[vXX.XX.XX]/bin/node`來執行，但在startup腳本中若用`npm start`，debian會從系統找全域設定的`npm`來執行，然後在打文章的當下突然想到說不定可以用`symlink`來解決！python的問題也類似。

## PM2
中間一度想用PM2執行兩個服務，這樣ssh登入主機時還可以監控log，但是跳的錯誤我一時沒辦法解決，就先放棄，之後再研究。

## 總結
其實不算太困難，主要是每測試一次腳本就必須重新開關主機，滿花時間。最後將路徑問題、套件相依和執行環境解決就成功了。但還有一些地方可以優化，像是`PM2`應該是更好的解決方案，以及執行環境也許可以用`symlink`，或者將系統的全域python和npm設定好，把腳本寫得更精簡。