---
layout: post
title: My Linux Command Challenge Solutions
subheading: Updating
author: cdxvy30
categories: markdown
tags: [linux, cli]
---
## TL;DR
只是記錄一下暑期實習第一週的小作業

## Challenges

1. Print "hello world" on the terminal in a single command.
```bash=
echo "hello world" # echo is like printf in c
```
2. Print the current working directory.
```bash=
pwd # print working directory
```
3. List names of all the files in the current directory, one file per line.
```bash=
ls <== list
```
4. Print the contents of file named `access.log`
```bash=
cat access.log
```
5. Print the last 5 lines of `access.log`
```bash=
tail -5 access.log
```
* [Reference](https://stackoverflow.com/questions/36989457/retrieve-last-100-lines-logs)
6. Create an empty file named `take-the-command-challenge` in the current working directory.
```bash=
touch take-the-command-challenge
```
7. Create a directory named `tmp/files` in the current working directory while `tmp` doesn't exist.
```bash=
mkdir -p tmp/files
```
* [Reference](https://unix.stackexchange.com/questions/84191/how-to-create-nested-directory-in-a-single-command)
8. Copy the file named `take-the-command-challenge` to the directory `tmp/files`.
```bash=
cp take-the-command-challenge tmp/files
```
* [Reference](https://blog.gtwang.org/linux/linux-cp-command-copy-files-and-directories-tutorial/)
9. Move the file named `take-the-command-challenge` to the directory `tmp/files`.
```bash=
mv take-the-command-challenge tmp/files
```
10. Create a symbolic link named `take-the-command-challenge` that points to the file `tmp/files/take-the-command-challenge`.
```bash=
ln -s tmp/files/take-the-command-challenge take-the-command-challenge
```
* [Reference](https://linuxize.com/post/how-to-create-symbolic-links-in-linux-using-the-ln-command/)
* Hard Link

11. Delete all of the files in this challenge directory including all subdirectories and their contents.
```bash=
find -delete
rm -r .* * 
```
```bash=
rm -r .* # -r代表遞迴
==> rm: refusing to remove '.' or '..' directory: skipping '.'
==> rm: refusing to remove '.' or '..' directory: skipping '..'
```
  * [How to delete all files in a current directory starting with a dot](https://unix.stackexchange.com/questions/310754/how-to-delete-all-files-in-a-current-directory-starting-with-a-dot)
  * This topic costs me a lot of time, need to research other ways
  * [How to merge multiple commands: Use `;`](https://www.howtogeek.com/269509/how-to-run-two-or-more-terminal-commands-at-once-in-linux/).
  * `find` command?

12. Remove all files with the `.doc` extension recursively in the current wirking diretory.
```bash=
find . -name "*.doc" -type f -delete
```
* [Reference](https://linuxhint.com/remove-all-files-with-extension-linux-command-line/)

13. Print all lines in `access.log` that contains the string "GET".
```bash=
grep GET access.log
```
* [Reference](https://blog.gtwang.org/linux/linux-grep-command-tutorial-examples/)
* `grep` command?
* `grep`常用地方:每日擷取日誌(log)內特定字串，搭配正規表達式。

14. Print all files in the current directory, one per line (not the path, just the filename) that contain the string "500".
```bash=
grep -Ril 500
```
* When trying : 
```bash=
grep -E 500 # command time out
grep -R 500 # Will print the matched content in the file.
```

15. Print the relative file paths, one path per line for all filenames that start with "access.log" in the current directory.
```bash=
ls access.*
ls
```

16. Print all matching lines (without the filename or the file path) in all files under the current directory that start with "access.log" that contain the string "500".
```bash=
grep -hr 500
```

17. Extract all IP addresses from files that start with "access.log" printing one IP address per line.
```bash=
grep -ro '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}'
```

18. Count the number of files in the current working directory. Print the number of files as a single integer.
```bash=
ls -l | wc -l
```

19. Print the contents of access.log sorted. 
```bash=
sort access.log
```

20. Print the number of lines in access.log that contain the string "GET".
```bash=
grep GET access.log | wc -l
```

21. The file split-me.txt contains a list of numbers separated by a ; character. Split the numbers on the `;` character, one number per line.
```bash=
cat split-me.txt | tr ';' '\n'
```

22. Print the numbers 1 to 100 separated by spaces.
```bash=
echo {1..100}
```

23. This challenge has text files (with a .txt extension) that contain the phrase "challenges are difficult". Delete this phrase from all text files recursively. `hint`: Note that some files are in subdirectories so you will need to search for them.
```bash=
sed -i s/challenges are difficult//g **/*.txt 
```

24. The file `sum-me.txt` has a list of numbers, one per line. Print the sum of these numbers.
```bash=
awk '{ sum += $1 } END { print sum }' sum-me.txt
```

25. Print all files in the current directory recursively without the leading directory path.
```bash=
find . -type f -printf "%f\n"
ls -r | grep [a-z]
```
