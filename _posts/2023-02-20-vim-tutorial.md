---
layout: post
title: Dev.Diary-輕量化文字編輯器:學習用vim提升工作效率
subheading: Updating
author: cdxvy30
categories: develop
tags: [linux, tool]
---
## 前言
就是紀錄一下vim的概念跟常用指令，因為真的很好用。

引用成大資工黃敬群(jserv)教授的話:
> vim 是強大的IDE，很多初學者碰到 vim 會很不習慣，像是用 hjkl 鍵來移動游標，不小心按到方向鍵讓編輯一整個崩潰的悲慘經驗，使得初學者不喜歡碰 vim (被視窗化介面給寵壞了)。 vim 強大的地方在於使用者可以自由的設定和擴充功能，讓 vim 為自己工作，進而擁有良好得編輯體驗。良好的 vim 設定，使得在 coding 過程中不用去煩惱編輯器的問題，增強 coding 效率。"

## 安裝
- MacOS 和 Linux 預設安裝`vi`，`vim`則是`vi`的加強版
- 執行`sudo apt install vim`
- 執行`vim`若有文字編輯器畫面出現即安裝成功

## 模式
- 命令模式(command-line mode)
- 輸入模式(insert mode)
- 視覺模式(visual mode)

## 自定義配置(.vimrc)
最基本的vim環境其實滿陽春的，沒有行號、highlight等支援，不過這麼強大的工具當然可以透過一些配置來讓開發體驗更好的。透過`.vimrc`設定檔，

```bash
set nu
set cursorline
set tabstop=4
set shiftwidth=4

" Color configuration
set bg=dark
color evening  " Same as :colorscheme evening
hi LineNr cterm=bold ctermfg=DarkGrey ctermbg=NONE
hi CursorLineNr cterm=bold ctermfg=Green ctermbg=NONE
```

## 總結